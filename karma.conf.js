/* eslint-env node*/

module.exports = function(config) {
  'use strict';

  config.set({

    basePath: './',

    // Longer timeout duration (15min)
    browserNoActivityTimeout: 15 * 60 * 1000,

    files: [
    ],

    proxies: {
      '/node_modules': '/base/node_modules',
      '/base': '/base/app',
    },

    jspm: {
      serveFiles: [
        'node_modules/babel-core/browser.js',
        'app/**/*.js',
      ],
      loadFiles: [
        'tests/cards/*.js',
        'tests/klondike/**/*.js',
      ],
    },

    autoWatch: true,

    frameworks: ['jspm', 'jasmine'],

    browsers: ['PhantomJS2'],

    plugins: [
      'karma-jspm',
      'karma-phantomjs2-launcher',
      'karma-jasmine',
      'karma-junit-reporter',
    ],

    junitReporter: {
      outputFile: 'test_out/unit.xml',
      suite: 'unit',
    },

  });
};
